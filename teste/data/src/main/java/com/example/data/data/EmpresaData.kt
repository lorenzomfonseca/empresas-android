package com.example.data.data

import com.example.domain.entidades.Empresa
import com.google.gson.annotations.SerializedName

data class EmpresaData(
    @SerializedName("id")
    val id: Int,
    @SerializedName("email_enterprise")
    var emailEnterprise: String?,
    @SerializedName("facebook")
    var facebook: String?,
    @SerializedName("twitter")
    var twitter: String?,
    @SerializedName("linkedin")
    var linkedin: String?,
    @SerializedName("phone")
    var phone: String?,
    @SerializedName("own_enterprise")
    var ownEnterprise: Boolean?,
    @SerializedName("enterprise_name")
    var enterpriseName: String?,
    @SerializedName("photo")
    var photo: String?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("city")
    var city: String?,
    @SerializedName("country")
    var country: String?,
    @SerializedName("value")
    var value: Int?,
    @SerializedName("share_price")
    var sharePrice: Double?,
    @SerializedName("enterprise_type")
    var enterpriseType: TipoEmpresaData,
    @SerializedName("errors")
    var listaErros : List<String>?
) {
}

fun EmpresaData.converterParaEmpresa() =
    Empresa(
        id = id,
        emailEnterprise = emailEnterprise,
        facebook = facebook,
        twitter = twitter,
        linkedin = linkedin,
        phone = phone,
        ownEnterprise = ownEnterprise,
        enterpriseName = enterpriseName,
        photo = photo,
        description = description,
        city = city,
        country = country,
        value = value,
        sharePrice = sharePrice,
        enterpriseType = enterpriseType.converterParaTipoEmpresa()

    )

fun List<EmpresaData>.converterParaListaEmpresas() =
    this.map { empresa -> empresa.converterParaEmpresa() }.toList()



