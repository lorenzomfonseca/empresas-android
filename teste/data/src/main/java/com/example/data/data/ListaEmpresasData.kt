package com.example.data.data

import com.example.domain.entidades.ListaEmpresas
import com.google.gson.annotations.SerializedName

data class ListaEmpresasData(
    @SerializedName("enterprises")
    var listaEmpresas: List<EmpresaData>?,
    @SerializedName("errors")
    var listaErros: List<String>?
)

fun ListaEmpresasData.converterParaListaEmpresas() =
    ListaEmpresas(listaEmpresas?.converterParaListaEmpresas(), listaErros)