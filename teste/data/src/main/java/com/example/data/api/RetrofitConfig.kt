package com.example.data.api

import android.content.Context
import com.example.data.util.Constantes
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitConfig {
    companion object{
        fun getRetrofitInstance(context : Context) : Retrofit {
            return Retrofit.Builder()
                .baseUrl(Constantes.ParametrosApi.DEV_HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }
}