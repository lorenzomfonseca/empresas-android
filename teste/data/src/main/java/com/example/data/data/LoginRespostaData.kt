package com.example.data.data

import com.example.domain.entidades.LoginResposta
import com.google.gson.annotations.SerializedName

data class LoginRespostaData(
    @SerializedName("investor")
    var investidor: InvestidorData?,
    @SerializedName("enterprise")
    var empresa: EmpresaData?,
    @SerializedName("success")
    var sucesso: Boolean?
)

fun LoginRespostaData.converterParaLoginResposta() =
    LoginResposta(
        investor = investidor?.converterParaInvestidor(),
        empresa = empresa?.converterParaEmpresa(),
        sucesso = sucesso
    )