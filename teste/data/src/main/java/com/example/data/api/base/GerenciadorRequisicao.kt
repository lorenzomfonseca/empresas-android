package com.example.data.api.base

import android.content.res.Resources
import android.security.keystore.UserNotAuthenticatedException
import com.example.data.api.base.excecoes.ApiException
import com.example.data.api.base.excecoes.RepositorioException
import com.example.data.api.base.excecoes.SucessoComRetornoNuloException
import retrofit2.Response
import java.util.concurrent.TimeoutException

object GerenciadorRequisicao {

    @Throws(RepositorioException::class, SucessoComRetornoNuloException::class)
    fun <T> obterResposta(response: Response<T>): T {


        return obterRespostaNullable(response) ?: throw SucessoComRetornoNuloException()
    }

    @Throws(RepositorioException::class)
    fun <T> obterRespostaNullable(response: Response<T>): T? {
        if (response.isSuccessful) {
            return response.body()
        } else {
            when (response.code()) {
                401 -> throw  UserNotAuthenticatedException()
                404 -> throw Resources.NotFoundException()
                504 -> throw TimeoutException()
            }

            throw ApiException("Ocorreu um erro com a API")
        }
    }
}