package com.example.data.data

import com.google.gson.annotations.SerializedName

data class EmpresaRespostaData(
    @SerializedName("enterprise")
    var empresa: EmpresaData?
)

fun EmpresaRespostaData.converterParaEmpresa() = empresa?.converterParaEmpresa()