package com.example.data.util

object Constantes {
    object ParametrosApi{
        const val ACCESS_TOKEN : String = "access-token"
        const val UID : String = "uid"
        const val CLIENTE : String =  "client"
        const val DATA_EXPIRACAO : String = "expiry"
        const val DEV_HOST : String = "https://empresas.ioasys.com.br"
        const val API_VERSION : String = "v1"

    }
}