package com.example.data.data

import com.example.domain.entidades.Investidor
import com.google.gson.annotations.SerializedName

data class InvestidorData(
    @SerializedName("id")
    val id: Int,
    @SerializedName("investor_name")
    var investorName: String?,
    @SerializedName("email")
    var email: String?,
    @SerializedName("city")
    var city: String?,
    @SerializedName("country")
    var country: String?,
    @SerializedName("balance")
    var balance: Double?,
    @SerializedName("photo")
    var photo: String?,
    @SerializedName("portfolio")
    var portfolio: PortfolioData?,
    @SerializedName("portfolio_value")
    var valorPortfolio: Double?,
    @SerializedName("first_access")
    var primeiroAcesso: Boolean?,
    @SerializedName("super_angel")
    var superAnjo: Boolean?
)

fun InvestidorData.converterParaInvestidor() = Investidor(
    id = id,
    investorName = investorName,
    email = email,
    city = city,
    country = country,
    balance = balance,
    photo = photo,
    portfolio = portfolio?.converterParaPortfolio(),
    valorPortfolio = valorPortfolio,
    primeiroAcesso = primeiroAcesso,
    superAnjo = superAnjo
)