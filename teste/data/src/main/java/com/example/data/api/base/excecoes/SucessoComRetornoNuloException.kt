package com.example.data.api.base.excecoes

class SucessoComRetornoNuloException(message: String? = null, cause: Throwable? = null) : ApiException(message, cause)