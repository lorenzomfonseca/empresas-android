package com.example.data.repositorios

import com.example.data.api.EmpresaApi
import com.example.data.api.base.GerenciadorRequisicao
import com.example.data.api.base.excecoes.ApiException
import com.example.data.api.base.excecoes.RepositorioException
import com.example.data.data.LoginRequisicaoData
import com.example.data.data.converterParaEmpresa
import com.example.data.data.converterParaListaEmpresas
import com.example.data.data.converterParaLoginResposta
import com.example.domain.entidades.Empresa
import com.example.domain.entidades.ListaEmpresas
import com.example.domain.entidades.LoginRequisicao
import com.example.domain.entidades.LoginResposta
import com.example.domain.repositorios.IEmpresasRepositorio
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class EmpresaRepositorio(private val api: EmpresaApi) : IEmpresasRepositorio {
    private val TAG: String = "FALHA_REPOSITORIO_EMPRESAS"
    override suspend fun realizarLogin(loginRequisicao: LoginRequisicao): LoginResposta? {
        return withContext(Dispatchers.IO) {
            try {
                val resposta = GerenciadorRequisicao.obterResposta(
                    api.realizarLogin(
                        LoginRequisicaoData(loginRequisicao.email, loginRequisicao.senha)
                    ).execute()
                )

                resposta.converterParaLoginResposta()

            } catch (excecao: ApiException) {
                val mensagem = "Falha ao efetuar login com os seguintes parâmetros."
                val complemento =
                    "Parametros: ${loginRequisicao.email}. Exceção: ${excecao}."
                throw RepositorioException(TAG, mensagem, complemento, excecao)
            }
        }

    }

    override suspend fun listarEmpresas(tipoEmpresa: Int?, nomeEmpresa: String?): ListaEmpresas? {
        return withContext(Dispatchers.IO) {
            try {
                val resposta = GerenciadorRequisicao.obterResposta(
                    api.listarEmpresas(tipoEmpresa, nomeEmpresa).execute()
                )

                resposta.converterParaListaEmpresas()
            } catch (excecao: ApiException) {
                val mensagem = "Falha ao obter empresas com os seguintes parâmetros."
                val complemento =
                    "Parametros: ${tipoEmpresa}\n ${nomeEmpresa}. Exceção: ${excecao}."
                throw RepositorioException(TAG, mensagem, complemento, excecao)
            }

        }
    }

    override suspend fun listarEmpresaPorId(id: Int?): Empresa? {
        return withContext(Dispatchers.IO) {
            try {
                val resposta =
                    GerenciadorRequisicao.obterResposta(api.listarEmpresaPorId(id).execute())
                resposta.empresa?.converterParaEmpresa()
            } catch (excecao: ApiException) {
                val mensagem = "Falha ao obter empresa com o seguinte id:"
                val complemento =
                    "Parametros: ${id}. Exceção: ${excecao}."
                throw RepositorioException(TAG, mensagem, complemento, excecao)
            }
        }
    }
}