package com.example.data.api

import com.example.data.data.EmpresaRespostaData
import com.example.data.data.ListaEmpresasData
import com.example.data.data.LoginRequisicaoData
import com.example.data.data.LoginRespostaData
import com.example.data.util.Constantes
import retrofit2.Call
import retrofit2.http.*

interface EmpresaApi {



    @POST("/api/${Constantes.ParametrosApi.API_VERSION}/users/auth/sign_in")
    fun realizarLogin(
        @Body loginRequisicaoData: LoginRequisicaoData
    ): Call<LoginRespostaData>

    @GET("/api/${Constantes.ParametrosApi.API_VERSION}/enterprises")
    fun listarEmpresas(
        @Query("enterprise_types") tipoEmpresaCodigo: Int?,
        @Query("name") nomeEmpresa: String?
    ): Call<ListaEmpresasData>

    @GET("/api/${Constantes.ParametrosApi.API_VERSION}/enterprises/{id}")
    fun listarEmpresaPorId(@Path("id") id: Int?): Call<EmpresaRespostaData>



}