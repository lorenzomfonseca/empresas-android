package com.example.data.data

import com.example.domain.entidades.TipoEmpresa
import com.google.gson.annotations.SerializedName

data class TipoEmpresaData(
    @SerializedName("id")
    val id: Int,
    @SerializedName("enterprise_type_name")
    var enterpriseTypeName: String?
)

fun TipoEmpresaData.converterParaTipoEmpresa() =
    TipoEmpresa(id = id, enterpriseTypeName = enterpriseTypeName)