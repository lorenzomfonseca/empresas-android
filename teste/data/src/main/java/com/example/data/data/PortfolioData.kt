package com.example.data.data

import com.example.domain.entidades.Portfolio
import com.google.gson.annotations.SerializedName

class PortfolioData(
    @SerializedName("enterprises_number")
    var numeroDeEmpresas: Int?,
    @SerializedName("enterprises")
    var empresas: List<EmpresaData>
)

fun PortfolioData.converterParaPortfolio() =
    Portfolio(numeroDeEmpresas, empresas.converterParaListaEmpresas())

