package com.example.data.data

import com.google.gson.annotations.SerializedName

data class LoginRequisicaoData(
    @SerializedName("email")
    var email : String?,
    @SerializedName("password")
    var senha : String?) {
}

