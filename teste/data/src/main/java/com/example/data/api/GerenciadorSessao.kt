package com.example.data.api

import android.app.Application
import android.content.SharedPreferences
import com.example.data.util.Constantes

class GerenciadorSessao(val app: Application) {
    val sharedPreferences: String = "shared_preferences"

    fun limparDados() {
        val sharedPrefs: SharedPreferences = app.getSharedPreferences(sharedPreferences, 0)

        val editor: SharedPreferences.Editor = sharedPrefs.edit()

        editor.clear()
        editor.apply()
    }

    fun salvarInformacoesUsuario(
        accessToken: String,
        uid: String,
        client: String,
        dataExpiracao: String
    ) {
        val sharedPrefs: SharedPreferences = app.getSharedPreferences(sharedPreferences, 0)
        val editor = sharedPrefs.edit()
        editor.putString(Constantes.ParametrosApi.ACCESS_TOKEN, accessToken)
        editor.putString(Constantes.ParametrosApi.CLIENTE, client)
        editor.putString(Constantes.ParametrosApi.UID, uid)
        editor.putString(Constantes.ParametrosApi.DATA_EXPIRACAO, dataExpiracao)
        editor.apply()

    }

    fun retornarAccessToken(): String? {
        val sharedPrefs: SharedPreferences = app.getSharedPreferences(sharedPreferences, 0)
        return sharedPrefs.getString(Constantes.ParametrosApi.ACCESS_TOKEN, "")
    }

    fun retornarClient(): String? {
        val sharedPrefs: SharedPreferences = app.getSharedPreferences(sharedPreferences, 0)
        return sharedPrefs.getString(Constantes.ParametrosApi.CLIENTE, "")
    }

    fun retornarUid(): String? {
        val sharedPrefs: SharedPreferences = app.getSharedPreferences(sharedPreferences, 0)
        return sharedPrefs.getString(Constantes.ParametrosApi.UID, "")
    }

    fun retornarExpiryDate(): String? {
        val sharedPrefs: SharedPreferences = app.getSharedPreferences(sharedPreferences, 0)
        return sharedPrefs.getString(Constantes.ParametrosApi.DATA_EXPIRACAO, "")
    }

    fun headersValidos() : Boolean{
        return  !retornarClient().isNullOrBlank() && !retornarAccessToken().isNullOrBlank() && !retornarUid().isNullOrBlank()
    }


}