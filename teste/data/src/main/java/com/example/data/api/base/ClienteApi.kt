import android.app.Application
import com.example.data.api.GerenciadorSessao
import com.example.data.api.base.excecoes.GsonUtil
import com.example.data.util.Constantes
import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

class ClienteApi(app : Application) {
    private  val TIMEOUT_SECONDS: Long = 300
    private var retrofit: Retrofit? = null
    private var retrofitToken: Retrofit? = null
    private val gerenciadorSessao = GerenciadorSessao(app)



    private val clientToken: Retrofit
        get() {
            if (retrofitToken == null) {
                val httpClient = OkHttpClient.Builder()
                httpClient.readTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
                httpClient.callTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)


                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BODY
                httpClient.addInterceptor(logging)


            }
            return retrofitToken as Retrofit
        }

    private val client: Retrofit
        get() {
            if (retrofit == null) {
                val httpClient = OkHttpClient.Builder()
                httpClient.readTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
                httpClient.callTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)

                httpClient.addInterceptor(object : Interceptor {
                    override fun intercept(chain: Interceptor.Chain): Response {
                        val originalRequest = chain.request()

                        val requestBuilder =
                            originalRequest.newBuilder().header("Authorization", "auth-value")

                        requestBuilder.addHeader(
                            "access-token",
                            gerenciadorSessao.retornarAccessToken()!!
                        )
                        requestBuilder.addHeader("client", gerenciadorSessao.retornarClient()!!)
                        requestBuilder.addHeader("uid", gerenciadorSessao.retornarUid()!!)

                        val request = requestBuilder.build()
                        val response = chain.proceed(request)

                        val allHeaders: Headers = response.headers
                        val accessToken = allHeaders.get("access-token")
                        val client = allHeaders.get("client")
                        val uid = allHeaders.get("uid")
                        val expiry = allHeaders.get("expiry")

                        gerenciadorSessao.salvarInformacoesUsuario(
                            accessToken!!,
                            uid!!,
                            client!!,
                            expiry!!
                        )

                        return response


                    }
                })

                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BODY
                httpClient.addInterceptor(logging)



                retrofit = Retrofit.Builder()
                    .baseUrl(Constantes.ParametrosApi.DEV_HOST)
                    .addConverterFactory(GsonConverterFactory.create(GsonUtil.gsonDefault))
                    .client(httpClient.build())
                    .build()
            }
            return retrofit as Retrofit
        }

    fun <S> createService(serviceClass: Class<S>): S {
        return client.create(serviceClass)
    }


    private fun configurarDataExpiracao(valorEmSegundos: Int): Date {
        val cal = Calendar.getInstance()
        cal.add(Calendar.SECOND, valorEmSegundos)

        return cal.time
    }
}