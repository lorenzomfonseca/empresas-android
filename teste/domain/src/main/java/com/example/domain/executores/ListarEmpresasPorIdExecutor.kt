package com.example.domain.executores

import com.example.domain.base.GerenciadorErroExecutor
import com.example.domain.base.IExecutor
import com.example.domain.base.TagExcecao
import com.example.domain.entidades.Empresa
import com.example.domain.repositorios.IEmpresasRepositorio

class ListarEmpresasPorIdExecutor(private val repositorio: IEmpresasRepositorio) :
    IExecutor<Empresa?, ListarEmpresasPorIdExecutor.Parametros> {
    data class Parametros(val id: Int)

    override suspend fun executar(parametros: Parametros): Empresa? {
        try {
            val resposta = repositorio.listarEmpresaPorId(parametros.id)
            return resposta
        } catch (excecao: Exception) {
            throw GerenciadorErroExecutor.tratarExcecao(
                TagExcecao.LISTAR_EMPRESAS_POR_ID,
                excecao
            )
        }
    }
}