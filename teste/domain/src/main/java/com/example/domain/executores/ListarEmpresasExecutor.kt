package com.example.domain.executores

import com.example.domain.base.GerenciadorErroExecutor
import com.example.domain.base.IExecutor
import com.example.domain.base.TagExcecao
import com.example.domain.entidades.Empresa
import com.example.domain.entidades.ListaEmpresas
import com.example.domain.repositorios.IEmpresasRepositorio

class ListarEmpresasExecutor(private val repositorio: IEmpresasRepositorio) :
    IExecutor<ListaEmpresas?, ListarEmpresasExecutor.Parametros> {
    data class Parametros(val tipoEmpresa: Int?, val nomeEmpresa: String?)

    override suspend fun executar(parametros: Parametros): ListaEmpresas? {
        try {
            val resposta = repositorio.listarEmpresas(
                tipoEmpresa = parametros.tipoEmpresa,
                nomeEmpresa = parametros.nomeEmpresa
            )


            return resposta
        } catch (excecao: Exception) {
            throw GerenciadorErroExecutor.tratarExcecao(
                TagExcecao.LISTAR_EMPRESAS,
                excecao
            )
        }
    }
}