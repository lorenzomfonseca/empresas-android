package com.example.domain.repositorios

import com.example.domain.entidades.Empresa
import com.example.domain.entidades.ListaEmpresas
import com.example.domain.entidades.LoginRequisicao
import com.example.domain.entidades.LoginResposta

interface IEmpresasRepositorio {
    suspend fun realizarLogin(loginRequisicao : LoginRequisicao) : LoginResposta?
    suspend fun listarEmpresas(tipoEmpresa: Int?, nomeEmpresa: String?) : ListaEmpresas?
    suspend fun listarEmpresaPorId(id : Int?) : Empresa?
}