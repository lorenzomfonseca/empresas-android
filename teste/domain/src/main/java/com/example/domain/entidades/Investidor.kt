package com.example.domain.entidades

data class Investidor(
    val id: Int,
    var investorName: String?,
    var email: String?,
    var city: String?,
    var country: String?,
    var balance: Double?,
    var photo: String?,
    var portfolio: Portfolio?,
    var valorPortfolio: Double?,
    var primeiroAcesso: Boolean?,
    var superAnjo : Boolean?
) {
}