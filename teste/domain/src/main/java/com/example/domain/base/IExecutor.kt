package com.example.domain.base

interface IExecutor<T, Params> {
    suspend fun executar(parametros: Params): T
}

interface IExecutorSemParametro<T> {
    suspend fun executar(): T
}