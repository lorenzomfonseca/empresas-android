package com.example.domain.base

enum class TagExcecao(val valor : String) {
    NAO_IDENTIFICADO("NAO_IDENTIFICADO"),
    REALIZAR_LOGIN("REALIZAR_LOGIN"),
    LISTAR_EMPRESAS("LISTAR_EMPRESAS"),
    LISTAR_EMPRESAS_POR_ID("LISTAR_EMPRESAS_POR_ID")
}