package com.example.domain.entidades

data class Empresa(
    val id: Int,
    var emailEnterprise: String?,
    var facebook : String?,
    var twitter : String?,
    var linkedin : String?,
    var phone : String?,
    var ownEnterprise : Boolean?,
    var enterpriseName : String?,
    var photo : String?,
    var description : String?,
    var city : String?,
    var country : String?,
    var value : Int?,
    var sharePrice : Double?,
    var enterpriseType : TipoEmpresa
    ) {
}
