package com.example.domain.entidades

data class LoginResposta(
    var investor: Investidor?,
    var empresa: Empresa?,
    var sucesso: Boolean?
) {
}