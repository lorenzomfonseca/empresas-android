package com.example.empresasandroid.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.SearchView.OnQueryTextListener
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.data.api.GerenciadorSessao
import com.example.domain.entidades.Empresa
import com.example.empresasandroid.R
import com.example.empresasandroid.ui.BaseActivity
import com.example.empresasandroid.ui.adapters.EmpresasAdapter
import com.example.empresasandroid.ui.dialogs.DialogBuilder
import com.example.empresasandroid.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {
    val gerenciadorSessao : GerenciadorSessao = get()
    val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        when (gerenciadorSessao.headersValidos()) {
            true -> {
                setContentView(R.layout.activity_main)
                configurarProgressBar()
                configurarListeners()
                configurarObservers()
            }
            false -> {
                val loginIntent = Intent(this, LoginActivity::class.java)
                startActivity(loginIntent)
            }

        }
    }

    private fun buscarEmpresas(tipoEmpresaCodigo: Int?, nomeEmpresa: String?) {
        doAsyncWork(work = {
            mainViewModel.buscarPorEmpresas(
                tipoEmpresaCodigo = tipoEmpresaCodigo,
                nomeEmpresa = nomeEmpresa
            )
        })
    }

    private fun configurarListeners() {

        menu_search_view.setOnQueryTextListener(object : OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                iniciar_busca.visibility = View.GONE
                menu_search_view.esconderTeclado()
                buscarEmpresas(null, query)

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })

        botao_ativar_busca.setOnClickListener {
            configurarAtivacaoBusca()
        }

        val searchCloseButtonId = menu_search_view.context.resources.getIdentifier(
            "android:id/search_close_btn",
            null,
            null
        )
        val closeButton = menu_search_view.findViewById(searchCloseButtonId) as ImageView

        closeButton.setOnClickListener {

            configurarFechamentoBusca()
        }


    }




    private fun configurarObservers() {
        configurarObserverListaEmpresas()
    }

    private fun configurarObserverListaEmpresas() {
        mainViewModel.listaEmpresas.observe(this, Observer { listaEmpresas ->
            listaEmpresas?.listaEmpresas?.let {
                if (it.isNotEmpty()) configurarEmpresasRecyclerView(it) else configurarListaEmpresasVazia()
            }

            listaEmpresas?.listaErros?.let {
                if (it[0] == R.string.erro_nao_autorizado.toString()) realizarlogout()
            }


        })
    }

    private fun realizarlogout() {
        gerenciadorSessao.limparDados()
        DialogBuilder(this).exibirDialogUmBotao(
            getString(R.string.erro),
            getString(R.string.erro_sem_credenciais),
            getString(R.string.ok)
        ) {
            val loginIntent = Intent(this, LoginActivity::class.java)
            startActivity(loginIntent)
        }
    }

    private fun configurarProgressBar() {
        viewProgressBar = progressBar
    }

    private fun configurarEmpresasRecyclerView(listaEmpresas: List<Empresa>?) {
        iniciar_busca.visibility = View.GONE
        textview_semResultados.visibility = View.GONE
        val empresasRecyclerView = recyclerview_Empresas
        empresasRecyclerView.visibility = View.VISIBLE
        empresasRecyclerView.adapter = EmpresasAdapter(listaEmpresas!!, application)
        val layoutManager = LinearLayoutManager(application)
        empresasRecyclerView.layoutManager = layoutManager


    }

    fun View.esconderTeclado() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun configurarListaEmpresasVazia() {
        iniciar_busca.visibility = View.GONE
        recyclerview_Empresas.visibility = View.GONE
        textview_semResultados.visibility = View.VISIBLE
    }

    private fun configurarFechamentoBusca(): Boolean {
        Log.v("fechamento", "chamado")
        menu_search_view.visibility = View.GONE
        logo_mainActivity.visibility = View.VISIBLE
        botao_ativar_busca.visibility = View.VISIBLE
        return true
    }

    private fun configurarAtivacaoBusca() {
        menu_search_view.visibility = View.VISIBLE
        logo_mainActivity.visibility = View.GONE
        botao_ativar_busca.visibility = View.GONE
    }

}
