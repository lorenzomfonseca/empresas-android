package com.example.empresasandroid.di.modulos

import com.example.domain.executores.ListarEmpresasExecutor
import com.example.domain.executores.ListarEmpresasPorIdExecutor
import com.example.domain.executores.RealizarLoginExecutor
import org.koin.dsl.module

internal val ModulosDomain = module {
    factory { ListarEmpresasPorIdExecutor(get()) }
    factory { ListarEmpresasExecutor(get()) }
    factory { RealizarLoginExecutor(get()) }
}