package com.example.empresasandroid

import android.app.Application
import com.example.empresasandroid.di.ModulosApp
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class EmpresasAndroid : Application() {

    override fun onCreate() {

        super.onCreate()
        setupKoin()

    }
    private fun setupKoin() {
        startKoin {
            androidContext(this@EmpresasAndroid)
            modules(
                ModulosApp.obterModulos()
            )
        }
    }
}