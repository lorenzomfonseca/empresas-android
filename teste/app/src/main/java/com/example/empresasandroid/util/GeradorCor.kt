package com.example.empresasandroid.util

import android.graphics.Color
import java.util.*


class GeradorCor {
    fun gerarCorAleatoria(): Int {
        val random = Random()
        val color: Int =
            Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256))

        return color
    }
}