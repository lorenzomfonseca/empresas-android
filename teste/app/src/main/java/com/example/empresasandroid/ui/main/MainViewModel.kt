package com.example.empresasandroid.ui.main

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.entidades.Empresa
import com.example.domain.entidades.ListaEmpresas
import com.example.domain.executores.ListarEmpresasExecutor

class MainViewModel(
    private val listarEmpresasExecutor: ListarEmpresasExecutor,
    val app: Application
) : ViewModel() {
    val nomeEmpresa = MutableLiveData<String>()
    val listaEmpresas: MutableLiveData<ListaEmpresas?> by lazy {
        MutableLiveData<ListaEmpresas?>()
    }

    suspend fun buscarPorEmpresas(tipoEmpresaCodigo: Int?, nomeEmpresa: String?) {
        val params = ListarEmpresasExecutor.Parametros(null, nomeEmpresa)
        val resposta = listarEmpresasExecutor.executar(params)


        listaEmpresas.postValue(resposta)
    }

}