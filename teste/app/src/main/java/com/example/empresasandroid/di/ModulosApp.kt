package com.example.empresasandroid.di

import com.example.empresasandroid.di.modulos.ModulosData
import com.example.empresasandroid.di.modulos.ModulosDomain
import com.example.empresasandroid.di.modulos.ModulosUI
import com.example.empresasandroid.di.modulos.ModulosUtil
import org.koin.core.module.Module

object ModulosApp {
    fun obterModulos(): List<Module> = listOf(
        ModulosUI,
        ModulosData,
        ModulosDomain,
        ModulosUtil
    )
}