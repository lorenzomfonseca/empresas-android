package com.example.empresasandroid.di.modulos

import com.example.empresasandroid.ui.detalhesempresa.DetalhesEmpresaViewModel
import com.example.empresasandroid.ui.login.LoginViewModel
import com.example.empresasandroid.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val ModulosUI = module {
    viewModel { LoginViewModel(get(), get()) }
    viewModel { MainViewModel(get(), get()) }
    viewModel { DetalhesEmpresaViewModel(get(), get()) }
}