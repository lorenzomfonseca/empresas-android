package com.example.empresasandroid.ui.login

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.lifecycle.Observer
import com.example.data.api.GerenciadorSessao
import com.example.empresasandroid.R
import com.example.empresasandroid.ui.BaseActivity
import com.example.empresasandroid.ui.dialogs.DialogBuilder
import com.example.empresasandroid.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : BaseActivity() {
    private var mUltimoClickBotaoLogin = 0L
    private val realizarLoginViewModel: LoginViewModel by viewModel()
    private val gerenciadorSessao: GerenciadorSessao = get()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        configurarProgressBar()
        configurarListeners()
        configurarObservers()

    }


    private fun realizarLogin(email: String, senha: String) {
        doAsyncWork(work = { realizarLoginViewModel.realizarLogin(email = email, senha = senha) })
    }

    private fun configurarListeners() {
        botao_efetuar_login.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mUltimoClickBotaoLogin > 1000) {
                mUltimoClickBotaoLogin = SystemClock.elapsedRealtime()
                if (realizarLoginViewModel.formularioValido.value!!) {
                    realizarLogin(
                        realizarLoginViewModel.email.value!!,
                        realizarLoginViewModel.senha.value!!
                    )

                } else {
                }

            }
        }

        input_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editavel: Editable?) {
            }

            override fun beforeTextChanged(valor: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(valor: CharSequence?, p1: Int, p2: Int, p3: Int) {
                valor?.let {
                    when (valor.isBlank()) {
                        false -> realizarLoginViewModel.email.postValue(valor.toString())
                        else -> doNothing()
                    }

                }
            }

            private fun doNothing() {

            }
        })
        input_senha.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editavel: Editable?) {
            }

            override fun beforeTextChanged(valor: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(valor: CharSequence?, p1: Int, p2: Int, p3: Int) {
                valor?.let {
                    when (valor.isBlank()) {
                        false -> realizarLoginViewModel.senha.postValue(valor.toString())
                        else -> doNothing()
                    }
                }
            }

            private fun doNothing() {

            }
        })
    }

    private fun configurarObservers() {
        prepararObserverLoginResposta()
        prepararObserverEmail()
        prepararobserverSenha()
        prepararObserverFormularioValido()
    }

    private fun prepararObserverLoginResposta() {
        realizarLoginViewModel.loginResposta.observe(this, Observer { loginResposta ->
            Log.v("LoginResposta", "Algo mudou")
            Log.v("LoginResposta", loginResposta.toString())
            loginResposta?.let {
                when (it.sucesso) {
                    true -> redirecionarParaMainActivity()
                    false -> exibirMensagemErroLogin()
                }

                Log.v("loginSucesso", it.sucesso.toString())

            }
        })
    }

    private fun prepararObserverFormularioValido() {
        realizarLoginViewModel.formularioValido.observe(this, Observer { valido ->
            botao_efetuar_login.isEnabled = when (valido) {
                true -> true
                false -> false
            }
            Log.v("formularioValido", valido.toString())
        })
    }

    private fun prepararObserverEmail() {
        realizarLoginViewModel.email.observe(this, Observer { email ->
            Log.v("email", "${email}")
            realizarLoginViewModel.formularioValido()
        })

    }

    private fun prepararobserverSenha() {
        realizarLoginViewModel.senha.observe(this, Observer { senha ->
            Log.v("senha", "${senha}")
            realizarLoginViewModel.formularioValido()
        })
    }


    private fun configurarProgressBar() {
        viewProgressBar = progressBar
    }

    private fun redirecionarParaMainActivity() {
        val loginIntent = Intent(this, MainActivity::class.java)
        startActivity(loginIntent)
        finish()
    }

    private fun exibirMensagemErroLogin() {
        DialogBuilder(this).exibirDialogUmBotao(
            getString(R.string.erro),
            getString(R.string.algo_de_errado),
            getString(R.string.ok)
        ) { }
    }
}
