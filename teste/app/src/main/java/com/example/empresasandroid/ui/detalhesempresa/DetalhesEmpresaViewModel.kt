package com.example.empresasandroid.ui.detalhesempresa

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.entidades.Empresa
import com.example.domain.executores.ListarEmpresasPorIdExecutor

class DetalhesEmpresaViewModel(
    private val listarEmpresasPorIdExecutor: ListarEmpresasPorIdExecutor,
    val app: Application
) : ViewModel() {
    val nomeEmpresa = MutableLiveData<String>()
    val fotoEmpresa = MutableLiveData<String>()
    val descricaoEmpresa = MutableLiveData<String>()
    val localizacaoEmpresa = MutableLiveData<String>()
    val tipoEmpresa = MutableLiveData<String>()
    val sharePriceEmpresa = MutableLiveData<Double>()
    val empresa: MutableLiveData<Empresa?> by lazy {
        MutableLiveData<Empresa?>()
    }

    suspend fun carregarDados(empresaId: Int) {
        val params = ListarEmpresasPorIdExecutor.Parametros(id = empresaId)
        val resposta = listarEmpresasPorIdExecutor.executar(params)

        empresa.postValue(resposta)
    }

}