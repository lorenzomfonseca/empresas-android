package com.example.empresasandroid.ui.detalhesempresa

import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.empresasandroid.R
import com.example.empresasandroid.ui.BaseActivity
import com.example.empresasandroid.util.GeradorCor
import kotlinx.android.synthetic.main.activity_detalhes_empresa.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetalhesEmpresaActivity : BaseActivity() {
    val empresaDetalhesEmpresaViewModel: DetalhesEmpresaViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhes_empresa)
        configurarComponentes()
        buscarEmpresasDetalhes(intent.getIntExtra("empresa_id", 1))
        configurarObservers()
    }

    private fun buscarEmpresasDetalhes(empresaId: Int) {
        doAsyncWork(work = {
            empresaDetalhesEmpresaViewModel.carregarDados(
                empresaId = empresaId
            )
        })
    }

    private fun configurarObservers() {
        configurarObserverDescricaoEmpresa()
        configurarObserverEmpresaDetalhes()
        configurarObserverNomeEmpresa()
        configurarObserverFotoEmpresa()
        configurarObserverSharePrice()
        configurarObserverTipoEmpresa()
    }

    private fun configurarObserverEmpresaDetalhes() {
        empresaDetalhesEmpresaViewModel.empresa.observe(this, Observer { empresa ->
            empresa?.let {
                empresaDetalhesEmpresaViewModel.nomeEmpresa.postValue(empresa.enterpriseName)
                empresaDetalhesEmpresaViewModel.descricaoEmpresa.postValue(empresa.description)
                empresaDetalhesEmpresaViewModel.fotoEmpresa.postValue(empresa.photo)
                empresaDetalhesEmpresaViewModel.sharePriceEmpresa.postValue(empresa.sharePrice)
                empresaDetalhesEmpresaViewModel.tipoEmpresa.postValue(empresa.enterpriseType.enterpriseTypeName)

                logo_empresaDetalhes.text = configurarIniciais(empresa.enterpriseName!!)
                logo_empresaDetalhes.setBackgroundColor(GeradorCor().gerarCorAleatoria())
                localizacao_empresaDetalhes.text = "Location: ${empresa.city}, ${empresa.country}"

            }
        })
    }

    private fun configurarObserverNomeEmpresa() {
        empresaDetalhesEmpresaViewModel.nomeEmpresa.observe(this, Observer {
            nome_empresaDetalhes.text = it
        })
    }

    private fun configurarObserverDescricaoEmpresa() {
        empresaDetalhesEmpresaViewModel.descricaoEmpresa.observe(this, Observer {
            descricao_empresaDetalhes.text = it
        })
    }

    private fun configurarObserverFotoEmpresa() {
        empresaDetalhesEmpresaViewModel.nomeEmpresa.observe(this, Observer {

        })
    }

    private fun configurarObserverTipoEmpresa() {
        empresaDetalhesEmpresaViewModel.tipoEmpresa.observe(this, Observer {
            tipo_empresaDetalhes.text = "Sector: ${it}"
        })
    }

    private fun configurarObserverSharePrice() {
        empresaDetalhesEmpresaViewModel.sharePriceEmpresa.observe(this, Observer {
            sharePrice_empresaDetalhes.text = "Share price: \$${it.toInt()}"
        })


    }

    private fun configurarProgressBar() {
        viewProgressBar = progressBar
    }

    private fun configurarComponentes() {
        configurarProgressBar()
        prepararToolbar()
    }

    private fun configurarIniciais(nome: String): String {
        var iniciais: String? = ""
        val nomes = nome.split(" ")
        for (n: String in nomes) {
            iniciais += n.substring(0, 1)
        }

        return iniciais!!.toUpperCase()

    }

    private fun prepararToolbar() {
        val toolbar = toolbar_empresaDetalhes
        toolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_left_black_24dp)
        toolbar.title = ""
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            finish()
        }
    }

}
