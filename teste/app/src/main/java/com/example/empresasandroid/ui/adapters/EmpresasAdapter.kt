package com.example.empresasandroid.ui.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.entidades.Empresa
import com.example.empresasandroid.R
import com.example.empresasandroid.ui.detalhesempresa.DetalhesEmpresaActivity
import com.example.empresasandroid.util.GeradorCor
import kotlinx.android.synthetic.main.empresa_item.view.*

class EmpresasAdapter(private val empresas : List<Empresa>, private val context : Context) : RecyclerView.Adapter<EmpresasAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val nomeEmpresaTextView = itemView.textview_nomeEmpresa
        val tipoEmpresaTextView = itemView.textview_tipoEmpresa
        val localizacaoEmpresaTextView = itemView.textview_localizacaoEmpresa
        val sharepriceEmpresaTextView = itemView.textview_sharepriceEmpresa
        val empresaCardView = itemView.cardview_empresa

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.empresa_item, parent, false)


        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return empresas.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val empresa = empresas[position]
        holder?.let{
            holder.nomeEmpresaTextView.text = empresa.enterpriseName
            holder.nomeEmpresaTextView.setBackgroundColor(GeradorCor().gerarCorAleatoria())
            holder.tipoEmpresaTextView.text = empresa.enterpriseType.enterpriseTypeName
            holder.localizacaoEmpresaTextView.text = empresa.country
            holder.sharepriceEmpresaTextView.text = "\$${empresa.sharePrice?.toInt()}"

        }

        holder.empresaCardView.setOnClickListener {
            val detalhesIntent = Intent(context, DetalhesEmpresaActivity::class.java)
            detalhesIntent.putExtra("empresa_id", empresa.id)
            detalhesIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(detalhesIntent)
        }



    }

}