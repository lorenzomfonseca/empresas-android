package com.example.empresasandroid.ui.splashscreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.empresasandroid.R
import com.example.empresasandroid.ui.main.MainActivity

class SplashActivity: AppCompatActivity() {
    private val SPLASH_TIME_OUT:Long=3000
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({


            startActivity(Intent(this, MainActivity::class.java))

            // close this activity
            this.finish()
        }, SPLASH_TIME_OUT)
    }
}