package com.example.empresasandroid.di.modulos

import com.example.data.api.GerenciadorSessao
import org.koin.dsl.module

internal val ModulosUtil = module {
    single {
        GerenciadorSessao(get())
    }
}