package com.example.empresasandroid.util

object Constantes {
    object Parametros{
        const val ACCESS_TOKEN : String = "access-token"
        const val UID : String = "uid"
        const val CLIENTE : String =  "client"
        const val DATA_EXPIRACAO: String = "expiry"
        const val DEV_HOST : String = "https://empresas.ioasys.com.br"
        const val API_VERSION : String = "v1"
    }

    object Valores {
        var ACCESS_TOKEN_VALOR: String? = null
        var UID_VALOR: String? = null
        var CLIENT_VALOR: String? = null
        var EXPIRY_DATE_VALOR: String? = null
    }
}