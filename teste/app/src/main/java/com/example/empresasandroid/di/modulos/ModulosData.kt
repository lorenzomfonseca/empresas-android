package com.example.empresasandroid.di.modulos

import ClienteApi
import com.example.data.api.EmpresaApi
import com.example.data.repositorios.EmpresaRepositorio
import com.example.domain.repositorios.IEmpresasRepositorio
import org.koin.dsl.bind
import org.koin.dsl.module

internal val ModulosData = module {
    single { ClienteApi(get()).createService(EmpresaApi::class.java) }
    factory { EmpresaRepositorio(get()) } bind IEmpresasRepositorio::class

}