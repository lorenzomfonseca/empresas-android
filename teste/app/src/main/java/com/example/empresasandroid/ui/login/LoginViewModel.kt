package com.example.empresasandroid.ui.login

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.entidades.LoginRequisicao
import com.example.domain.entidades.LoginResposta
import com.example.domain.executores.RealizarLoginExecutor

class LoginViewModel(
    private val realizarLoginExecutor: RealizarLoginExecutor,
    val app: Application
) : ViewModel() {
    val email = MutableLiveData<String?>()
    val senha = MutableLiveData<String?>()
    val formularioValido = MutableLiveData<Boolean>().apply { value = false }
    val loginResposta: MutableLiveData<LoginResposta?> by lazy {
        MutableLiveData<LoginResposta?>()
    }

    suspend fun realizarLogin(email: String, senha: String) {
        val params = RealizarLoginExecutor.Parametros(LoginRequisicao(email = email, senha = senha))
        val resposta = realizarLoginExecutor.executar(params)
        Log.v("sucesso", resposta?.sucesso.toString())
        Log.v("empresa", resposta?.empresa.toString())
        Log.v("empresario", resposta?.investor?.investorName.toString())
        this.loginResposta.postValue(resposta)
    }

    fun formularioValido() {
        formularioValido.postValue(emailValido() && senhaValida())
    }

    fun emailValido(): Boolean {
        return when (email.value.isNullOrBlank()) {
            true -> false
            false -> email.value!!.contains("@") && email.value!!.contains(".com")
        }
    }

    fun senhaValida(): Boolean {
        return when (senha.value.isNullOrBlank()) {
            true -> false
            false -> senha.value!!.length >= 8
        }
    }
}