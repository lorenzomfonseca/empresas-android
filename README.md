

## Empresas ##

Estes documento README tem como objetivo falar sobre o que foi realizado no projeto Empresas, desafio técnico proposto pela Ioasys.

### Como executar? ###

* Para executar este projeto, é necessário possuir o Android Studio ou alguma ferramenta que permita rodar uma APK ou projeto Android.
* Depois de realizado o fork/clonagem do projeto, deverá abrir o projeto "teste" dentro do Android Studio.
* A partir disso, depois que feito as atualizações da ferramenta Gradle, o projeto poderá ser rodado normalmente.

### Bibliotecas utilizadas ###

* Coroutines(1.3.3) : Ferramenta utilizada para fazer chamadas em threads leves e não-bloqueadoras, aprimorando performance.
* Koin(2.0.1): Utilizada para injeção de dependências nos executores, viewModels, API e afins.
* Android Jetpack(2.2.0) : Utilização de componentes como ViewModel e LiveData, para abstrair a camada de visualização e adequar ao modelo MVVM de arquitetura.
* Glide(0.9.2): Utilizada para fazer o cache de imagens.
* OkHttp(3.8.0) : Ferramenta utilizada para estabelecer um cliente HTTP robusto.
* Retrofit(2.6.1) e GSON: Ferramenta utilizada para facilitar obtenção de objetos através de parse de JSON após chamdas de API.

### Se houvesse mais tempo... ###


* Teria investido mais tempo em um gerenciador de sessão mais robusto; como há validade no token, seria bom para aprimorar a experiência de renovação do mesmo sem sair do fluxo, comecei esta parte mas não concluí.;
* Construiria o botão de toggle para visualização da senha;
* Teria trabalhado o foco da searchview situada na página de buscas do aplicativo;
* Faria utilização de imagem teria sido feito da melhor forma, fazendo uma verificação mais contundente
* Teria Populado com mais informações a página de detalhes da empresa; Coloquei algumas, mas creio que valia colocar mais
* Teria feito um rodapé navegador na parte de baixo do app, onde o usuário poderia alternar entre visualizar empresas e visualizar o próprio perfil
* Teria melhorado o filtro da página de buscas; atualmente, só é possível buscar por nome, mas se soubesse todos os tipos de empresas, poderia fazer um filtro para isto, também.
* Teria trabalhado com mais paciência na UI da página de detalhes da empresa.


### Considerações pessoais ###
* Utilizei um padrão de modularidade voltado para desenvolvimento em MVVM; Talvez seja muito robusto para um projeto pequeno, mas gosto de pensar que os projetos sempre podem ser expandidos no futuro, por isso mantive o desenvolvimento em três módulos(app, data, domain).



